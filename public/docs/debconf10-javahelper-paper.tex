\documentclass[letter,12pt]{article}

\usepackage{palatino}
\usepackage{listings}
\usepackage{fullpage}

\title{Packaging with Javahelper}

\author {
	Matthew Johnson mjj29@debian.org
}

\date {
	1st August 2010
}

\setcounter{secnumdepth}{-1}

\newenvironment{block}[1]%
{\begin{figure}[!htb]\caption{#1}}%
{\end{figure}}

\begin{document}

\maketitle

\begin{abstract}

Programs and libraries written in Java have a lot of special packaging
requirements in common. When I started packaging Java programs I was
annoyed at the lack of tools to automate some of these tasks and hence
Javahelper was born.

Javahelper provides tools to help with the work flow right from processing
upstream releases (which can be tricky with some Java upstreams) through
building packages where upstream doesn't have a sane build system to 
generating packaging meta-data and installing files into packages.

This talk gives an overview of the DH7 and CDBS integration, how to use the
various helper tools available in javahelper and finishes with example packages
of a selection of simple Java programs and libraries.

The aim of Javahelper is to provide tooling to implement Debian Java policy,
automate common tasks required by Java packages that aren't provided for by
debhelper and to simplify the packaging of Java software.

\end{abstract}

\section{Upstream}
\subsection{Upstream releases}

   \begin{itemize}
      \item Lots of jar or zip releases not tars
      \item Releases include 3rd party libraries
      \item Releases include build products (javadoc, class files, etc)
      \item Repacking needs to be done with every new release
   \end{itemize}
   \begin{itemize}
      \item Solution: jh\_repack
   \end{itemize}

jh\_repack will remove any .class files, any .jar files, the whole directory
tree containing javadoc and any empty directories as a result of the above.

   \begin{block}{jh\_repack watch file}
\begin{lstlisting}
version=3
http://www.matthew.ath.cx/projects/salliere/ \
 (?:.*/)?salliere-?_?([\d+\.]+|\d+)\.(jar|zip) \
 debian jh_repack
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_repack manual}
\begin{lstlisting}
jh_repack --upstream-version <version> <archive>
\end{lstlisting}
   \end{block}

\subsection{Upstream build systems}

   \begin{itemize}
      \item Build system expects 3rd party libraries in the build tree
      \item Build system broken
      \item Build system non-existent
      \item Build system doesn't build javadoc
   \end{itemize}
   \begin{itemize}
      \item Solution: jh\_linkjars
      \item Solution: jh\_build
   \end{itemize}


   \begin{block}{jh\_linkjars (old-style)}
\begin{lstlisting}
jh_linkjars libs
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_linkjars (debian/linkjars)}
\begin{lstlisting}
libs
\end{lstlisting}
   \end{block}
jh\_linkjars will scan all of the build dependencies and then symlink every
jar which the dependency installs to \verb&/usr/share/java& into the target directory.
The directory should either be passed on the command line and then again with
-u in the clean target, or put in the file \verb&debian/linkjars&.

   \begin{block}{jh\_build (old-style)}
\begin{lstlisting}
JAVA_HOME=/usr/lib/jvm/default-java
CLASSPATH=/usr/share/java/esd.jar:...
jh_build weirdx.jar src
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_build (DH7---debian/javabuild)}
\begin{lstlisting}
salliere.jar src
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_build (DH7---debian/rules)}
\begin{lstlisting}
export JAVA_HOME=/usr/lib/jvm/default-java
export CLASSPATH=/usr/share/java/csv.jar:...
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_build (CDBS---debian/rules)}
\begin{lstlisting}
export JAVA_HOME=/usr/lib/jvm/default-java
export CLASSPATH=/usr/share/java/csv.jar:...
JH_BUILD_JAR=salliere.jar
JH_BUILD_SRC=src
\end{lstlisting}
   \end{block}

jh\_build is designed as a simple point-and-click build system for packages of
the form ``take all the .java files in that directory, compile them to .class
files and put them in a .jar".  This is the case for many packages, especially
those without their own build system. More complex packages should use a full
build system such as make, ant or maven---and hopefully upstream will already
have provided these. 

Nevertheless there are still some options which can be provided to change how
it works. The CLASSPATH variable will be copied into the jar manifest
automatically, but the main-class attribute must be specified on the command
line. It can build javadoc automatically for you and both javac and javadoc can
have extra parameters passed to them via command line options to jh\_build.
Lastly, you can specify other non-class files to be included in the jar via an
environment variable. Using these command line parameters requires you to put
the source and jar files on the command line as well, and not use the
\verb&debian/javabuild& file.

Finally, for both jh\_linkjars and jh\_build there is a clean option to remove
build products, which is automatically called from jh\_clean, which you should
call from the clean target.

   \begin{itemize}
      \item Upstreams often fail to set manifest entries
      \item Debian-specific manifest entries may be needed
   \end{itemize}
   \begin{itemize}
      \item Solution: jh\_classpath
      \item Solution: jh\_manifest
   \end{itemize}

   \begin{block}{jh\_classpath (debian/classpath)}
\begin{lstlisting}
src/my.jar /usr/share/java/dep1.jar /usr/share/...
src/my-other.jar /usr/share/java/dep1.jar /...
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_manifest (debian/manifest)}
\begin{lstlisting}
usr/share/weirdx/weirdx.jar:
 Main-Class: com.jcraft.weirdx.WeirdX
 Debian-Java-Home: /usr/lib/jvm/default-java
\end{lstlisting}
   \end{block}
Both jh\_manifest and jh\_classpath can be called directly without
a file specifying everything on the command line or jh\_manifest
can be called to update every jar in the package with the classpath
taken from the CLASSPATH variable. These commands provide a simple, 
non-intrusive, way of providing common changes to the upstream build products.

As well as being used at runtime by the JVM to load dependent jars (which
works transitively through dependencies), the classpath entry can be used
to automatically populate the Depends line of your package. The use of the 
Debian- prefixed entries will be explained later.

\section{Installing}
   \begin{itemize}
      \item There are specific requirements for installation of jars and javadoc in policy
      \item Code duplication to support those requirements is bad
      \item Being able to make a single source change then rebuild to change the requirements is good
   \end{itemize}
   \begin{itemize}
      \item Solution: jh\_installlibs
      \item Solution: jh\_installjavadoc
   \end{itemize}
   \begin{block}{jh\_installlibs (debian/jlibs)}
\begin{lstlisting}
foo.jar
build/bar.jar
\end{lstlisting}
   \end{block}
   \begin{block}{jh\_installjavadoc (debian/javadoc)}
\begin{lstlisting}
doc/api
internal
api /usr/share/doc/libfoo-java/api
\end{lstlisting}
   \end{block}
jh\_installlibs will install a jar into \verb&/usr/share/java/$jar-$version.jar& with a symlink
from \verb&/usr/share/java/$jar.jar& as required by policy. It will attempt to strip the version number
from the jar and any DFSG suffix from the package version before installing it. Other version-mangling rules or the jars themselves can be provided on the command line.

jh\_installjavadoc has three forms. Calling it with a single path will install that path to 
\verb&/usr/share/doc/$package/api&. The destination can be overridden so that a -doc package can install
the doc in the directory for the library package (providing it depends on it). The final form
is the string ``internal" which will take the javadoc generated by jh\_build and install that
to the normal directory. In all cases it will generate a doc-base file to automatically register
the documentation with doc-base 

\section{Binary packages}
\subsection{Runtime support}
   \begin{itemize}
      \item Every java program needs a wrapper script
      \item Looking up the path to libjvm.so is hard
   \end{itemize}
   \begin{itemize}
      \item Solution: jarwrapper
      \item Solution: jh\_exec
      \item Solution: java-vars.sh
   \end{itemize}
jarwrapper installs a binfmt-misc handler for jar files
(technically, for zip files containing a manifest) which calls
\verb&java -jar $file&. A jar with a correct classpath and main class
entries in the manifest can just be made executable and to depend on
jarwrapper and in can run just like any other executable. There are
a few Debian-specific manifest entries which can be set which 
result in using a specific JVM or specific arguments being passed
to the JVM.

In order to make it easier to use the above, jh\_exec will scan
directories in the package which are on path and if any of those
contain symlinks to a .jar file it will make that file executable. 
This means that jars can be installed into \verb&/usr/share/java& or \verb&/usr/share/$package&
then dh\_link used to symlink them to \verb&/usr/bin&, then jh\_exec will
automatically set the modes.

Some applications need to load libjvm.so or other files from within
the JVM directory. These files are in architecture-specific directories
within the JVM, but those architectures do not match the debian architecture
name. To simplify finding these files for all packages there is a shell script
snippet (java-vars.sh) which will export a variety of variables based on
your JAVA\_HOME setting, shipped with jarwrapper for programs which need it at runtime.
This is also available as a makefile snipped in javahelper for use in \verb&debian/rules&
which need it at build time.

\subsection{Dependencies}
   \begin{itemize}
      \item Autogenerating Depends: lines (a-la dh\_shlibdeps) is helpful
      \item Policy specifies a variety of alternate depends on JREs
      \item jarwrapper is needed in the Depends for executable jars
      \item A single place that generates dependencies makes it easier to change policy
   \end{itemize}
   \begin{itemize}
      \item Solution: jh\_depends
   \end{itemize}
dh\_shlibdeps automatically generates dependencies from the libraries
required by a C-library. jh\_depends attempts to do this for jar files
as well, using the classpath manifest entry in the jar. jh\_depends will
scan all the jars in your package, work out which jars each depends on
and which packages contain those. These will then be added to the \${java:Depends}
substvar for replacing in control files. These dependencies will also include
other packages built from the same source in which case a strict versioned depends will
be used.

If your package contains any executable jar files, or if you explicitly request it,
jh\_depends will also create a dependency on jarwrapper in the first case and a 
JVM in either. The JVM will be taken from the Debian-Java-Home variable if it exists
in the manifest, or from the command line, defaulting to default-jre. Alternate depends
are constructed by scanning all the jars for classfile version and depending on a recent
enough virtual package to be able to run them.

If there are any policy changes regarding versioned dependencies (see my other talk)
then using jh\_depends will just require a rebuild and not any source changes.

\section{Eclipse}
\subsection{PDE based build}
   \begin{itemize}
		\item PDE-based build (auto-generates ant-build files)
		\item Clean does not clean properly.
   \end{itemize}
   \begin{itemize}
		\item Solution: jh\_setupenvironment
   \end{itemize}
jh\_setupenvironment copies (part of) the upstream sources into another directory that can
be discarded after the build. Slow, but it works. This is the same method used to ensure
a proper clean in the eclipse package.

jh\_clean removes the copy if it is in the default location.

\subsection{External (Orbit) dependencies}
   \begin{itemize}
		\item Eclipse plug-ins uses non eclipse libraries.
		\item Requires OSGi-metadata to be loaded (unless embedded)
		\item Lazy upstreams embeds the jar and use Bundle-Classpath
		\item Eclipse needs bundles named by their Symbolic Name.
		\item Eclipse fetches the bundles from Orbit.
		\item Setting up Orbit is a tedious task.
   \end{itemize}
   \begin{itemize}
      	\item Require-Bundle + OSGi-metadata in the Orbit dependency.
   		\item OSGi-metadata must be added manually, but is generally trivial to make.
		\item For everything else there is jh\_generateorbitdir.
   \end{itemize}
Given a list of jar files, jh\_generateorbitdir will setup the Orbit directory. It
reads every thing it needs from the Manifest of the jar file.

jh\_clean removes all temporary files and also the orbitdir if it is in the default location.

OSGi-metadata can usually be copied from Fedora or from Eclipse Orbit.

\subsection{Building features}
   \begin{itemize}
		\item Feature is a set of related plug-ins.
		\item Java sources are scattered neatly into separate plug-ins.
		\item Manifest files lists dependencies.
		\item The PDE handles all of this, but ...
		\item It takes over 10 arguments to make PDE build one ``trivial'' feature.
		\item LinuxTools/eclipse-build provides a wrapper, but it is still a pain. 
   \end{itemize}
   \begin{itemize}
		\item Solution: jh\_compilefeatures
   \end{itemize}
jh\_compilefeatures builds list of features given their ID and a list of the features
(or ``group of features'') they depend on. Unfortunately it does not resolve the feature
dependencies itself---most of the information is available, the code to collect it is
missing.

Currently it is a debhelper-compatible interface for the eclipse-build wrapper with a
little extra intelligence.

\subsection{Install features}
   \begin{itemize}
		\item Features are compiled into zip files.
		\item Must be unzipped and located so that eclipse finds it.
		\item The zip file contains a full copy of Orbit Dependencies.
   \end{itemize}
   \begin{itemize}
		\item Solution: jh\_installeclipse
   \end{itemize}
jh\_installeclipse just needs the ID of the feature that jh\_compilefeatures built and
a ``feature group'' to install it under (usually this can be guessed from the package
name). It will also replace Orbit Dependencies that jh\_generateorbitdir handled
with symlinks and populate \$\{orbit:Depends\} for your package.

Installs in beneath \verb&/usr/share/eclipse& for architecture independent packages and under
\verb&/usr/lib/eclipse& for other packages.

\section{Packaging frameworks}
\subsection{DH7}
   \begin{block}{DH7 debian/rules}
\begin{lstlisting}
#!/usr/bin/make -f

export JAVA_HOME=/usr/lib/jvm/default-java
export CLASSPATH=/usr/share/java/csv.jar:...

%:
	dh $@ --with javahelper
\end{lstlisting}
   \end{block}
javahelper works with DH7 using \verb&--with javahelper&. All the commands mentioned
above will be called in the correct order and will read from files under
\verb&debian/& without anything extra in your \verb&debian/rules&. In the case that you need
to supply extra arguments or otherwise change the behaviour, then override\_
stanzas can be used as normal.

\subsection{CDBS}
   \begin{block}{CDBS debian/rules}
\begin{lstlisting}
#!/usr/bin/make -f
export JAVA_HOME=/usr/lib/jvm/default-java
export CLASSPATH=/usr/share/java/csv.jar:...
JH_BUILD_JAR=salliere.jar
JH_BUILD_SRC=src

include /usr/share/CDBS/1/class/javahelper.mk
\end{lstlisting}
   \end{block}
javahelper is similarly integrated with CDBS, with the exception that jh\_build
is called using environment variables. Again, the normaly CDBS override methods
can be used, but there are also a number of variables which can be used to pass
arguments directly to programs instead.

\subsection{Package autogeneration}
jh\_makepkg can be used to generate most of a debian dir for a simple
Java application or a simple library. It will prompt for a few answers
it can't work out and infer some others.

\section{References}
More information about Javahelper can be found in the following locations:
   \begin{itemize}
      \item \verb&http://packages.debian.org/javahelper&
      \item \verb&/usr/share/doc/javahelper&
      \item \verb&http://pkg-java.alioth.debian.org/&
      \item \verb&http://pkg-java.alioth.debian.org/examples&
      \item \verb&debian-java@lists.debian.org&
   \end{itemize}

\end{document}
