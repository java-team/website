<?php

#$JDK6_DOWNLOAD_URL="http://www.oracle.com/technetwork/java/javase/documentation/overview-156328.html";
$JDK6_DOWNLOAD_URL="http://hg.openjdk.java.net/jdk6/jdk6/tags";
$JDK7_DOWNLOAD_URL="http://www.oracle.com/technetwork/java/javaseproducts/documentation/javase7supportreleasenotes-1601161.html";
$JDK8_DOWNLOAD_URL="http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html";
$JDK10_DOWNLOAD_URL="http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html";

$JAVA6_VERSION = trim(shell_exec("/usr/bin/wget -qO- $JDK6_DOWNLOAD_URL | /bin/sed -n 's/.*\jdk\(6-b[0-9]\+\).*/\\1/p' | /usr/bin/head -n 1"));
$JAVA7_VERSION = trim(shell_exec("/usr/bin/wget -qO- $JDK7_DOWNLOAD_URL | /bin/sed -n 's/.*\([7]\(u[0-9]\+\)\?-b[0-9]\+\).*/\\1/p' | /usr/bin/head -n 1"));
$JAVA8_VERSION = trim(shell_exec("/usr/bin/wget -qO- $JDK8_DOWNLOAD_URL | /bin/sed -n 's/.*\([8]\(u[0-9]\+\)\?-b[0-9]\+\).*/\\1/p' | /usr/bin/head -n 1"));
$JAVA10_VERSION = trim(shell_exec("/usr/bin/wget -qO- $JDK10_DOWNLOAD_URL | /bin/sed -n 's/.*jdk\/\(10[\.0-9]\++[0-9]\+\).*/\\1/p' | /usr/bin/head -n 1"));

?>

<html>
<head>
  <title>OpenJDK watch</title>
  <link href="https://www.debian.org/debian.css" rel="stylesheet" type="text/css">
</head>
<body>

<div id="content">

<h1>OpenJDK watch</h1>

<p>This page tracks the latest versions of Java released by Oracle. Note that
for a given update the Mercurial repository on the OpenJDK site will always
show more recent build numbers. Since they do not map to an actual release
they are ignored.</p>

<p>Latest Java releases:</p>

<ul>
  <li><a href="http://hg.openjdk.java.net/jdk-updates/jdk10u/archive/jdk-<?= $JAVA10_VERSION ?>.tar.gz">Java <?= $JAVA10_VERSION ?></a></li>
  <li><a href="http://hg.openjdk.java.net/jdk8u/jdk8u/archive/jdk<?= $JAVA8_VERSION ?>.tar.gz">Java <?= $JAVA8_VERSION ?></a></li>
  <li><a href="http://hg.openjdk.java.net/jdk7u/jdk7u/archive/jdk<?= $JAVA7_VERSION ?>.tar.gz">Java <?= $JAVA7_VERSION ?></a></li>
  <li><a href="http://hg.openjdk.java.net/jdk6/jdk6/archive/jdk<?= $JAVA6_VERSION ?>.tar.gz">Java <?= $JAVA6_VERSION ?></a></li>
</ul>

</div>

</body>
</html>
